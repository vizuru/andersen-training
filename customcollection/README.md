
#### Some info

- Collection is implemented as an expandable array. 

- Not all methods of List interface were implemented (just main ones)! Others throw UnsupportedOperationException.

- Tests also provided only for main methods. 

- NumberArrayList - same collection, but just for Number values and with additional getAverageValue() method.


---
Author: Repnin Siarhei, qvizuru@gmail.com.

