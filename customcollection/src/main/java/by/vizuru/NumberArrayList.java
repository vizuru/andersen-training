package by.vizuru;

/**
 * ArrayList just for Number types with additional operations of getting average value.
 *
 * @param <T> type of stored elements
 */
public class NumberArrayList<T extends Number> extends CustomArrayList<T> {

    public NumberArrayList() {
    }

    public NumberArrayList(int initCapacity) {
        super(initCapacity);
    }

    public double getAverageValue() {
        return getTotalAmount() / size();
    }

    private double getTotalAmount() {
        double totalAmount = 0;
        for (T t : this) {
            totalAmount += t.doubleValue();
        }
        return totalAmount;
    }
}
