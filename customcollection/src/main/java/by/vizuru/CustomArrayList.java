package by.vizuru;

import java.util.*;

/**
 * Collection that stores elements of the given type.
 * Implemented with expandable array.
 *
 * Note: Some unnecessary methods are not implemented!
 *
 * @param <T> stored values type
 */
public class CustomArrayList<T> implements List<T> {

    private Object[] array;
    private int size;

    private static final int INIT_CAPACITY = 10;

    /**
     * Constructs empty collection with given initial capacity.
     *
     * @param initCapacity initial capacity of the collection
     */
    public CustomArrayList(int initCapacity) {
        array = new Object[initCapacity];
        size = 0;
    }

    /**
     * Constructs empty collection with initial capacity (10).
     */
    public CustomArrayList() {
        this(INIT_CAPACITY);
    }

    /**
     * Returns size of the collection (number of elements).
     *
     * @return size of the collection (number of elements)
     */
    public int size() {
        return size;
    }

    /**
     * Checks if collection is empty.
     *
     * @return true if collection is empty
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Checks if given index is in bounds (0 <= index < size).
     *
     * @param index index to check
     * @return true if given index is in bounds
     */
    private boolean checkBounds(int index) {
        return index < size && index > -1;
    }

    /**
     * Checks if colection contains given object.
     *
     * @param obj object to check.
     * @return true if collection contains given element
     */
    public boolean contains(Object obj) {
        for (Object o : array) {
            if (o.equals(obj)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns collection as an array.
     *
     * @return collection as an array
     */
    public Object[] toArray() {
        return Arrays.copyOf(array, size);
    }


    /**
     * Stores collection int given array.
     *
     * @param a array to store elements into
     * @param <E> type of given array
     * @return given array filled with collection elements (if it can handle all the elements) or new array with
     * all collection elements
     */
    public <E> E[] toArray(E[] a) {
        if (a.length < size)
            // Make a new array of a's runtime type, but my contents:
            return (E[]) Arrays.copyOf(array, size, a.getClass());
        System.arraycopy(array, 0, a, 0, size);
        if (a.length > size)
            a[size] = null;
        return a;
    }

    /**
     * Adds given object into collection.
     *
     * @param t object to add
     * @return true if object was added
     */
    public boolean add(T t) {
        if (size < array.length) {
            array[size++] = t;
        } else {
            Object[] newArray = new Object[(size * 3) / 2 + 1];
            System.arraycopy(array,0, newArray, 0, size);
            array = newArray;
            array[size++] = t;
        }
        return true;
    }

    /**
     * Removes given element from collection.
     *
     * @param o object to remove
     * @return true if there's such object in collection and it was removed
     */
    public boolean remove(Object o) {
        int i = 0;
        while (i < size) {
            if (array[i].equals(o)) {
                System.arraycopy(array, i + 1, array, i, size - i - 1);
                size--;
                array[size] = null;
                return true;
            }
            i++;
        }
        return false;
    }

    private class CustomIterator implements Iterator<T> {
        private int pos = 0;

        @Override
        public T next() {
            if (!checkBounds(pos)) {
                throw new NoSuchElementException();
            }
            return (T) array[pos++];
        }

        @Override
        public boolean hasNext() {
            return pos != size;
        }
    }

    /**
     * Returns iterator for the collection.
     *
     * @return iterator for the collection
     */
    public Iterator<T> iterator() {
        return new CustomIterator();
    }

    /**
     * Clears all the elements of the collection.
     */
    public void clear() {
        for (int i = 0; i < size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    /**
     * Returns element of the collection with given index.
     *
     * @param index index of the element to get
     * @return element of the collection with given index
     */
    public T get(int index) {
        if (checkBounds(index)) {
            return (T) array[index];
        }
        throw new IndexOutOfBoundsException();
    }

    /**
     * Sets given element to the given position in the collection.
     *
     * @param index position to set element into
     * @param element element to set
     * @return stored element
     */
    public T set(int index, T element) {
        if (checkBounds(index)) {
            array[index] = element;
            return element;
        }
        throw new IndexOutOfBoundsException();
    }

    /**
     * Adds given element into collection by the given index.
     *
     * @param index index to store element by
     * @param element element to store
     */
    public void add(int index, T element) {
        if (index <= size && index > -1) {
            System.arraycopy(array, index, array, index + 1, size - index);
            array[index] = element;
            size++;
        } else {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Removes element by the given index.
     *
     * @param index index of the element to remove
     * @return removed element
     */
    public T remove(int index) {
        if (checkBounds(index)) {
            Object removed = array[index];
            System.arraycopy(array, index + 1, array, index, size - index - 1);
            size--;
            array[size] = null;
            return (T) removed;
        }
        throw new IndexOutOfBoundsException();
    }

    /**
     * Returns index of given object.
     *
     * @param o object to find
     * @return index of given object or -1 if object is not in the collection
     */
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++)
                if (array[i]==null)
                    return i;
        } else {
            for (int i = 0; i < size; i++)
                if (o.equals(array[i]))
                    return i;
        }
        return -1;
    }

    /**
     * Returns last index of given object.
     *
     * @param o object to find
     * @return last index of given object or -1 if object is not in the collection
     */
    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = size-1; i >= 0; i--)
                if (array[i]==null)
                    return i;
        } else {
            for (int i = size-1; i >= 0; i--)
                if (o.equals(array[i]))
                    return i;
        }
        return -1;
    }

    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException();
    }

    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException();
    }

    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(int index, Collection<? extends T> c) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }
}
