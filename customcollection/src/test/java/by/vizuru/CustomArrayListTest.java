package by.vizuru;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Note: contains tests only for main operations!
 */
public class CustomArrayListTest {

    private CustomArrayList<String> emptyCollection;
    private CustomArrayList<String> filledCollection;

    @BeforeEach
    public void setupEmptyCollection() {
        emptyCollection = new CustomArrayList<>(1);
    }

    @BeforeEach
    public void setupFilledCollection() {
        filledCollection = new CustomArrayList<>();
        filledCollection.add("So");
        filledCollection.add("awesome");
        filledCollection.add("collection");
        filledCollection.add("cantstand");
    }

    @Test
    public void whenAddElement_thenShouldAddAnd() {
        String e1 = "So";
        String e2 = "wow";
        String e3 = "collection";

        assertEquals(0, emptyCollection.size(), "Collection should be empty!");
        emptyCollection.add(e1);
        emptyCollection.add(e2);
        emptyCollection.add(e3);

        assertEquals(3, emptyCollection.size(), "Collection should have 3 elements!");
        assertEquals(e1, emptyCollection.get(0));
        assertEquals(e2, emptyCollection.get(1));
        assertEquals(e3, emptyCollection.get(2));
    }

    @Test
    public void whenRemoveObject_thenRemove() {
        filledCollection.remove("awesome");
        assertEquals(3, filledCollection.size(), "Collection should have 3 elements!");

        String removed = filledCollection.remove(1);
        assertEquals("collection", removed);

        removed = filledCollection.remove(1);
        assertEquals("cantstand", removed);
        assertEquals(1, filledCollection.size());
    }

    @Test
    public void whenRemoveMissingObject_thenReturnFalse() {
        boolean wasRemoved = filledCollection.remove("missing data");
        assertFalse(wasRemoved, "There's no element so it shouldn't be removed!");
    }

    @Test
    public void whenMoveWithIterator_thenMovesCorrectly() {
        emptyCollection.add("1");
        emptyCollection.add("2");
        emptyCollection.add("3");
        Iterator<String> it = emptyCollection.iterator();

        assertTrue(it.hasNext());
        assertEquals("1", it.next());
        assertEquals("2", it.next());
        assertTrue(it.hasNext());
        assertEquals("3", it.next());
        assertFalse(it.hasNext());
    }

    @Test
    public void whenAddByIndex_thenShouldAdd() {
        filledCollection.add(2, "duck");
        assertEquals(5, filledCollection.size());
        assertEquals("duck", filledCollection.get(2));
        assertEquals("collection", filledCollection.get(3));
        assertEquals("cantstand", filledCollection.get(4));
    }
}
